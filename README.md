Un Dashboard Sencillo donde se hace uso de los Iconos de Font Awesome para flutter
además, agregué un botón que redirecciona a una segunda pagina donde se pueden ver
unos gráficos estadisticos haciendo uso de el plugin Chart para Flutter.

PLugins - Librerias usadas

# Para que salgan cuadros bien alineados
  flutter_staggered_grid_view: ^0.3.0

# Para crear graficos y Charts
  flutter_sparkline: ^0.1.0
  flutter_circular_chart: ^0.1.0

# Para que se vean bonitos los iconos
  font_awesome_flutter: ^8.5.0


![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-27-48.png)
![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-27-52.png)
![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-27-58.png)
![Pantalla Inicial](assets/images/Screenshot_2019-11-24-15-28-02.png)

Todo hecho con Flutter 1.9 ..!
# dashboard_simple_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
