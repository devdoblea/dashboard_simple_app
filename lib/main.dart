import 'package:flutter/material.dart';
import 'package:dashboard_simple_app/pages/dashboard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple Dashboard',
      theme: ThemeData(
        primaryColor: new Color(0xFF622F74),
      ),
      home: Dashboard(),
    );
  }
}

