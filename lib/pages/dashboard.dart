import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:dashboard_simple_app/pages/estadisticas.dart';

class Dashboard extends StatefulWidget {
	@override
	_DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

	Material myItems(IconData icon, String heading, int color) {
		return Material(
			color: Colors.white,
			elevation: 14.0,
			shadowColor: Color(0x802196F3),
			borderRadius: BorderRadius.circular(24.0),
			child: Center(
				child: Padding(
					padding: const EdgeInsets.all(8.0),
					child: Row(
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget>[
							Column(
								mainAxisAlignment: MainAxisAlignment.center,
								children: <Widget>[
									Padding(
										padding: const EdgeInsets.all(8.0),
										child: Text(
											heading,
											style: TextStyle(
												color: new Color(color),
												fontSize: 20.0,
											),
										),
									),

									// Iconos
									Material(
										color: new Color(color),
										borderRadius: BorderRadius.circular(24.0),
										child: Padding(
											padding: const EdgeInsets.all(16.0),
											child: Icon(
												icon,
												color: Colors.white,
											),
										),
									),
								],
							),
						],
					),
				),
			),
		);
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu), 
          onPressed: () {},
        ),
				title: Text(
					'Dashboard',
					style: TextStyle(
						color: Colors.white,
					),
				),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.insert_chart),
            onPressed: () {
              //Definir este boton para agregar
              _verEstadisticas(EstadisticasWidget());
            },
          )
        ],
			),
			body: StaggeredGridView.count(
				crossAxisCount: 2,
				crossAxisSpacing: 12.0,
				mainAxisSpacing: 12.0,
				padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
				children: <Widget>[
					myItems(Icons.graphic_eq, "TotalViews",0xFFED622B),
					myItems(Icons.bookmark, "Bookmark",0xFF26CB3C),
					myItems(Icons.notifications, "Notifications",0xFFFF3266),
					myItems(Icons.attach_money, "Balance",0xFF3399FE),
					myItems(Icons.settings, "Settings",0xFFF4C83F),
					myItems(Icons.group_work, "Group Work",0xFF622F74),
					myItems(Icons.favorite, "Followers",0xFFAD61F1),
					myItems(Icons.message, "Messages",0xFF7297FF),
				],
				staggeredTiles: [
					StaggeredTile.extent(2, 130.0),
					StaggeredTile.extent(1, 250.0),
					StaggeredTile.extent(1, 130.0),
					StaggeredTile.extent(1, 130.0),
					StaggeredTile.extent(1, 150.0),
					StaggeredTile.extent(1, 130.0),
					StaggeredTile.extent(2, 240.0),
					StaggeredTile.extent(2, 120.0),
				],
			),
		);
	}

  void _verEstadisticas(metodo) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return metodo;
        },
      ),
    );
  }
}
